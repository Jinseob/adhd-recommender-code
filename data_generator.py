# -*- coding: utf-8 -*-
"""
Created on Tue May 30 10:45:28 2017

Synthetic data generation

@author: jin
"""

# 점수 기준
# CBCL :50-100.
# K-ADHDDS:0-130+.
# CARS:15-60. [정상:15-29.5, 경도&중간 자폐:30-36.5, 중증 자폐:37-60]
#   훈련 콘텐츠: 30개

import pandas as pd
import numpy as np
import pickle, itertools

# Set DataFrame with columns.
COLUMNS = ['만나이', '성별', 'CBCL', 'K-ADHDDS', 'CARS', '주의집중', '덕목', '주의력', '실행기능']
df = pd.DataFrame(columns=COLUMNS)

N_PATIENTS = 1234

# Append personal information
AGE = np.random.randint(low=9, high=14, size=N_PATIENTS)
AGE2 = np.around(np.random.ranf(size=N_PATIENTS), decimals=1)
age = AGE + AGE2
sex = np.random.randint(low=0, high=2, size=N_PATIENTS)
cbcl = np.random.randint(low=60, high=101, size=N_PATIENTS)
kadhdds = np.random.randint(low=80, high=141, size=N_PATIENTS)
cars = np.random.randint(low=28, high=37, size=N_PATIENTS)

df['만나이'] = age
df['성별'] = sex
df['CBCL'] = cbcl
df['K-ADHDDS'] = kadhdds
df['CARS'] = cars

df.fillna(value=0, inplace=True)

def MaxPick(types, series1, series2):
    result = pd.concat([series1, series2], axis=1)
    result[result<=0]=0
    if (types == 0): 
        result = result.max(axis=1)
    if (types == 1):
        result = np.where((result.ix[:,0]>=0) & (result.ix[:,1]>=0), result.max(axis=1), 0)
    return result

def Screening(series1, series2):
    result = pd.concat([series1, series2], axis=1)
    result = np.where((result.ix[:,0]>=0) & (result.ix[:,1]>=0), series2, 0)
    return pd.Series(result)

VAL_CBCL1 = 60
VAL_CBCL2 = 64
VAL_KADHDDS1 = 80
VAL_KADHDDS2 = 121
VAL_AGE = 9
VAL_CARS = 28

# Make condtions for 4 characters
# 일단 OR로 시작. 둘다 겹치면 둘 중 높은걸로.
CONDITION_CB1 = df['CBCL'] >= VAL_CBCL1    # 주의집중 조건
CONDITION_CB2 = df['CBCL'] >= VAL_CBCL2
CONDITION_K1 = df['K-ADHDDS'] >= VAL_KADHDDS1
CONDITION_K2 = df['K-ADHDDS'] >= VAL_KADHDDS2
CONDITION_AGE = df['만나이'] >= VAL_AGE
CONDITION_CARS = df['CARS'] >= VAL_CARS

# Interpolation
ag1 = df['만나이'].apply(lambda x: np.round((x - 9)*(1-0)/(14 - 9) + 0, decimals=1))
cb1 = df['CBCL'].apply(lambda x: np.round((x - 60)*(1-0)/(100 - 60) + 0, decimals=1))
cb2 = df['CBCL'].apply(lambda x: np.round((x - 64)*(1-0)/(100 - 64) + 0, decimals=1))
ad1 = df['K-ADHDDS'].apply(lambda x: np.round((x - 80)*(1-0)/(140 - 80) + 0, decimals=1))
ad2 = df['K-ADHDDS'].apply(lambda x: np.round((x - 121)*(1-0)/(140 - 121) + 0, decimals=1))
cr1 = df['CARS'].apply(lambda x: np.round((x - 28)*(1-0)/(36 - 28) + 0, decimals=1))

AND = 1
OR = 0

df['주의집중'] = MaxPick(OR, cb2, ad2)
df['덕목'] = Screening(ag1, cb1)
df['주의력'] = Screening(cb1, ad1)
df['실행기능'] = MaxPick(OR, Screening(cb1, ad1), Screening(cb1, cr1))
df[df<=0] = 0
df[['주의집중', '덕목', '주의력', '실행기능']] = np.round(df[['주의집중', '덕목', '주의력', '실행기능']], decimals=1)

N_CONTENTS = 30

COLUMNS2 = []
for I in np.arange(1,N_CONTENTS+1):
    COLUMNS2.append('콘텐츠_%d' %I)

df_contents = pd.DataFrame(columns=COLUMNS2)
df_contents = np.round(df_contents, decimals=0)
ftwt_ls = []
for i in np.arange(N_CONTENTS):
    ftwt = np.round(4*np.random.dirichlet(np.ones(4)), decimals=1)
    ftwt_ls.append(ftwt)
df_ftwt = pd.DataFrame(ftwt_ls)

df_contents = 1 + df_ftwt.dot(df[['주의집중', '덕목', '주의력', '실행기능']].T.reset_index(drop=True, inplace=False))
df_contents[(df_contents >= 5)] = 5
df_con_max = df_contents.max(axis=0)
df_con_min = df_contents.min(axis=0)

"""
# 파라미터 조합 수동 설정. 필요할 경우만 쓰시오.
tmp1=[]
for i in itertools.permutations([4,0,0,0]):
    tmp1.append(i)
tmp1 = pd.DataFrame(tmp1)
tmp1.drop_duplicates(inplace=True)

tmp2=[]
for i in itertools.permutations([2,2,0,0]):
    tmp2.append(i)
tmp2 = pd.DataFrame(tmp2)
tmp2.drop_duplicates(inplace=True)

tmp3=[]
for i in itertools.permutations([3,1,0,0]):
    tmp3.append(i)
tmp3 = pd.DataFrame(tmp3)
tmp3.drop_duplicates(inplace=True)
tmp4=[]
for i in itertools.permutations([2,1,1,0]):
    tmp4.append(i)
tmp4 = pd.DataFrame(tmp4)
tmp4.drop_duplicates(inplace=True)

tot = pd.concat([tmp1, tmp2, tmp3, tmp4, pd.DataFrame([1,1,1,1]).T], axis=0, ignore_index=True)
df_contents = 1 + tot.dot(df[['주의집중', '덕목', '주의력', '실행기능']].T.reset_index(drop=True, inplace=False))
"""

fname = 'user_profile'
with open(fname + '.pkl', 'wb') as f:
    pickle.dump([df, df_contents], f)