# -*- coding: utf-8 -*-
"""
Created on Wed May 31 18:01:29 2017

@author: HELPER
"""

import numpy as np
import pandas as pd
import pickle
from scipy.optimize import minimize
from sklearn.metrics import mean_squared_error
from math import sqrt

from cofiCostFunc import cofiCostFunc
from normalizeRatings import normalizeRatings

def MaxPick(types, series1, series2):
    result = pd.concat([series1, series2], axis=1)
    result[result<=0]=0
    if (types == 0): 
        result = result.max(axis=1)
    if (types == 1):
        result = np.where((result.ix[:,0]>=0) & (result.ix[:,1]>=0), result.max(axis=1), 0)
    return result

def Screening(series1, series2):
    result = pd.concat([series1, series2], axis=1)
    result = np.where((result.ix[:,0]>=0) & (result.ix[:,1]>=0), series2, 0)
    return pd.Series(result)

def rmse(prediction, ground_truth):
    prediction = prediction[ground_truth.nonzero()].flatten()
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))

def most_similar_users(person, number_of_users):
	# returns the number_of_users (similar persons) for a given specific person.
	scores = [("{:.5f}".format(pearson_correlation(person,other_person)), other_person) for other_person in df2 if other_person != person]
	
	# Sort the similar persons so that highest scores person will appear at the first
	scores.sort()
	scores.reverse()
	return scores[0:number_of_users]

def similarity_score(person1, person2):
    # Returns ratio Euclidean distance score of person1 and person2 
    both_viewed = {}		# To get both rated items by person1 and person2

    for item in df2[person1].index:
        if item in df2[person2].index:
            both_viewed[item] = 1

            # Conditions to check they both have an common rating items
            if len(both_viewed) == 0:
                return 0

            # Finding Euclidean distance 
            sum_of_eclidean_distance = []

            for item in df2[person1].index:
                if item in df2[person2].index:
                    sum_of_eclidean_distance.append(pow(df2[person1][item] - df2[person2][item],2))
            sum_of_eclidean_distance = sum(sum_of_eclidean_distance)

    return 1/(1+sqrt(sum_of_eclidean_distance))

def pearson_correlation(person1, person2):

	# To get both rated items
	both_rated = {}
	for item in df2[person1].index:
		if item in df2[person2].index:
			both_rated[item] = 1

	number_of_ratings = len(both_rated)
	
	# Checking for number of ratings in common
	if number_of_ratings == 0:
		return 0

	# Add up all the preferences of each user
	person1_preferences_sum = sum([df2[person1][item] for item in both_rated])
	person2_preferences_sum = sum([df2[person2][item] for item in both_rated])

	# Sum up the squares of preferences of each user
	person1_square_preferences_sum = sum([pow(df2[person1][item],2) for item in both_rated])
	person2_square_preferences_sum = sum([pow(df2[person2][item],2) for item in both_rated])

	# Sum up the product value of both preferences for each item
	product_sum_of_both_users = sum([df2[person1][item] * df2[person2][item] for item in both_rated])

	# Calculate the pearson score
	numerator_value = product_sum_of_both_users - (person1_preferences_sum*person2_preferences_sum/number_of_ratings)
	denominator_value = sqrt((person1_square_preferences_sum - pow(person1_preferences_sum,2)/number_of_ratings) * (person2_square_preferences_sum -pow(person2_preferences_sum,2)/number_of_ratings))
	if denominator_value == 0:
		return 0
	else:
		r = numerator_value/denominator_value
		return r

## ============== Part 1: Load data  & Appends new patient's data =============
print("-------------------------") 
print("      개인 정보 입력",end='\b')
print("\n-------------------------")
my_info = []
my_info.append(float(input("0. 성별(남1, 여0): ")))
my_info.append(float(input("1. 만나이: ")))
my_info.append(int(input("2. CBCL 점수: ")))
my_info.append(int(input("3. K-ADHDDS 점수: ")))
my_info.append(int(input("4. CARS 점수: ")))
my_info = pd.DataFrame(my_info, ['만나이', '성별', 'CBCL', 'K-ADHDDS', 'CARS']).T

AGE = my_info['만나이'].apply(lambda x: np.round((x - 9)*(1-0)/(14 - 9) + 0, decimals=1))
CBCL1 = my_info['CBCL'].apply(lambda x: np.round((x - 60)*(1-0)/(100 - 60) + 0, decimals=1))
CBCL2 = my_info['CBCL'].apply(lambda x: np.round((x - 64)*(1-0)/(100 - 64) + 0, decimals=1))
ADHD1 = my_info['K-ADHDDS'].apply(lambda x: np.round((x - 80)*(1-0)/(140 - 80) + 0, decimals=1))
ADHD2 = my_info['K-ADHDDS'].apply(lambda x: np.round((x - 121)*(1-0)/(140 - 121) + 0, decimals=1))
CARS = my_info['CARS'].apply(lambda x: np.round((x - 28)*(1-0)/(36 - 28) + 0, decimals=1))

AND = 1
OR = 0

my_info = pd.concat([my_info, MaxPick(OR, CBCL2, ADHD2), Screening(AGE, CBCL1), Screening(CBCL1, ADHD1), 
                     MaxPick(OR, Screening(CBCL1, ADHD1), Screening(CBCL1, CARS))], axis=1)
my_info.columns = ['만나이', '성별', 'CBCL', 'K-ADHDDS', 'CARS', '주의집중', '덕목', '주의력', '실행기능']
my_info[my_info<=0] = 0

with open('user_profile.pkl', 'rb') as f:
    Profile, Y = pickle.load(f)
Y = np.array(Y)
R = np.ones(Y.shape).astype(bool)

Profile = pd.concat([my_info, Profile], axis=0, ignore_index=True)

#  Create New patient data
my_ratings = np.zeros(len(Y))

print("\n-------------------------") 
print("     콘텐츠 점수 입력")
print(' 훈련:[1~30] | 점수:[1~5]')
print("-------------------------") 
CHK = 1
while(CHK == 1):
    tmp = input("훈련번호, 평점 : ").split()
    my_ratings[int(tmp[0])-1] = int(tmp[1])
    CHK = int(input("(완료=0 | 추가=1) : "))


#  Add our own ratings to the data matrix
Y = np.column_stack((my_ratings, Y))
R = np.column_stack((my_ratings, R)).astype(bool)

#  Normalize Ratings
Ynorm, Ymean = normalizeRatings(Y, R)

## ============== Part 2: Set some values & Gradient descents  ================
#  Set Values
N_USERS = Y.shape[1]
N_CONTENTS = Y.shape[0]
N_FEATURES = 4
LAMBDA = 10     #Regularization value

# Set Initial Parameters (Theta, X)
np.random.seed(42)
X = np.random.rand(N_CONTENTS, N_FEATURES)
Theta = np.random.rand(N_USERS, N_FEATURES)

#  Combine param X and Theta
initial_parameters = np.hstack((X.T.flatten(), Theta.T.flatten()))

#  Gradient descent
costFunc = lambda p: cofiCostFunc(p, Ynorm, R, N_USERS, N_CONTENTS, N_FEATURES, LAMBDA)[0]
gradFunc = lambda p: cofiCostFunc(p, Ynorm, R, N_USERS, N_CONTENTS, N_FEATURES, LAMBDA)[1]

print('\n---------------------------------------------')
result = minimize(costFunc, initial_parameters, method='CG', jac=gradFunc, options={'disp': True, 'maxiter': 1000.0})
theta = result.x
#cost = result.fun

# Unfold the returned theta back into U and W
X = theta[:N_CONTENTS*N_FEATURES].reshape(N_CONTENTS, N_FEATURES)
Theta = theta[N_CONTENTS*N_FEATURES:].reshape(N_USERS, N_FEATURES)

print ('\nRecommender system learning completed.')
print('---------------------------------------------')
## ================== Part 8: Recommendation for you ====================
#  After training the model, you can now make recommendations by computing
#  the predictions matrix.

p = X.dot(Theta.T)     
p = p + np.tile(Ymean, (p.shape[1], 1)).T

df_rate = pd.DataFrame(p)
df_ft = pd.DataFrame(X)
DataTable = pd.concat([df_ft, df_rate], axis=1)
PATIENT_COLUMNS = ["아동_%d" %NUM for NUM in np.arange(df_rate.shape[1]) + 1]
DataTable.columns = ['주의집중', '덕목', '주의력', '실행기능'] + PATIENT_COLUMNS

my_predictions = p[:, 0]

conList = ['콘텐츠_%d' %num for num in np.arange(1,len(Y)+1)]

# sort predictions descending
pre=np.array([[idx, p] for idx, p in enumerate(my_predictions)])
post = pre[pre[:,1].argsort()[::-1]]
r = post[:,1]
ix = post[:,0]

N_RECOMMEND = 10
TARGET_NAME = '아동1'
N_SIMILAR_USER = 10

# 콘텐츠 기반 추천 방식.
print ('\n%s의 콘텐츠 추천(콘텐츠 기반 추천 방식):' %TARGET_NAME)
for i in range(N_RECOMMEND):
    j = int(ix[i])
    print ('\t%s (%.1f)' % (conList[j], my_predictions[j]))


df2 = DataTable.ix[:,4:]
Profile2 = Profile.drop('성별', axis=1).T
Profile2.columns = PATIENT_COLUMNS
df2 = pd.concat([df2, Profile2], axis=0, ignore_index=True)

rank = most_similar_users('아동_1', N_SIMILAR_USER)
print('\n')

ls2 = [(rank[i][1], '콘텐츠_%d' %(DataTable[rank[i][1]].sort_values(ascending=False).index[0] + 1), DataTable[rank[i][1]].max(axis=0)) for i in np.arange(N_SIMILAR_USER)]
ls3 = [(rank[i][1], '콘텐츠_%d' %(DataTable[rank[i][1]].sort_values(ascending=False).index[1] + 1), DataTable[rank[i][1]].max(axis=0)) for i in np.arange(N_SIMILAR_USER)]
ls4 = [(rank[i][1], '콘텐츠_%d' %(DataTable[rank[i][1]].sort_values(ascending=False).index[2] + 1), DataTable[rank[i][1]].max(axis=0)) for i in np.arange(N_SIMILAR_USER)]
ls5 = [(rank[i][1], '콘텐츠_%d' %(DataTable[rank[i][1]].sort_values(ascending=False).index[3] + 1), DataTable[rank[i][1]].max(axis=0)) for i in np.arange(N_SIMILAR_USER)]
con2 = pd.DataFrame(ls2 + ls3 + ls4 + ls5)
con2.drop_duplicates(1, inplace=True)
print("%s의 추천 콘텐츠(유사 사용자 기반 맞춤형 추천 방식):" %TARGET_NAME)
for MAN, CONTEN, RATE in zip(list(con2[0])[:10], list(con2[1])[:10], list(con2[2])[:10]):
    print (' %s: %s (%.1f)' %(MAN, CONTEN, RATE))

Profile.index = ["아동_%d" %NUM for NUM in np.arange(df_rate.shape[1])]

del (theta, p, df_rate, df_ft, pre, post, r, ix, df2, tmp, ls2, ls3, ls4, ls5, con2, rank, i, j, result, Profile2)
